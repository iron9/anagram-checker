from anagram_checker.app import is_anagram


def test_anagram():
    assert is_anagram("erbgut", "geburt")


def test_anagram_different_letters():
    assert not is_anagram("hallo", "total")


def test_anagram_with_umlaut():
    assert is_anagram("hürde", "edrüh")


def test_anagram_with_space():
    assert is_anagram("erb gut", "geburt")


def test_anagram_with_duplicate_letters():
    assert is_anagram("Ratgeber", "ERGRABET")


def test_anagram_with_mixed_capitalization():
    assert is_anagram("H", "h")


def test_anagram_with_subset_of_characters():
    assert not is_anagram("erbe", "erb")


def test_anagram_with_subset_of_characters2():
    assert not is_anagram("erb", "erbe")


def test_anagram_long_example():
    assert is_anagram("TOM MARVOLO RIDDLE", "I AM LORD VOLDEMORT")


def test_anagram_with_punctuation():
    assert is_anagram("She Sells Sanctuary", "Santa; shy, less cruel")


def test_anagram_empty():
    assert is_anagram("", "")
