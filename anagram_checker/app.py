import re
from typing import Annotated

import uvicorn
from fastapi import FastAPI, Query
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def is_anagram(
    string1: Annotated[str, Query(example="TOM MARVOLO RIDDLE")],
    string2: Annotated[str, Query(example="I AM LORD VOLDEMORT")],
) -> bool:
    """
    Check whether two given strings are anagrams of each other.

    Two strings are anagrams of each other if one can be derived from
    the other by only re-arranging the characters. Note that capitalization,
    whitespace and punctuation are ignored.

    We perform this check by converting each string to lower case,
    sorting their characters, removing all whitespace and punctuation
    and then checking for equality.
    """
    p = re.compile(r"\W*")
    string1_sorted = "".join(sorted(string1.lower()))
    string2_sorted = "".join(sorted(string2.lower()))

    string1_only_chars = p.sub("", string1_sorted)
    string2_only_chars = p.sub("", string2_sorted)

    return string1_only_chars == string2_only_chars


def main():
    uvicorn.run("anagram_checker.app:app", reload=True)


if __name__ == "__main__":
    main()
