# Anagram checker

## Introduction

This is a simple service with one API endpoint:
It can determine whether two given strings are anagrams of each other.

See also the related frontend at <https://gitlab.com/iron9/anagram-checker-frontend>.

## How to run

1. Install python 3.11 (e.g. with [pyenv](https://github.com/pyenv/pyenv)).
2. Install poetry as explained in [the docs](https://python-poetry.org/docs/#installation)
3. Run `poetry install` to install the dependencies.
4. Run `poetry run start`. This will start a simple web server in reload mode.
5. The URL with which you can reach the API will be printed on the terminal.

For more information about how to run a FastAPI app check [its docs](https://fastapi.tiangolo.com/).

## API docs (OpenAPI)

Visit `<URL>/docs` to see the OpenAPI specification.
You can also interactively try out the API there.

## Tests

Run `poetry run pytest` to run the unit tests.

## Formatting

Run `poetry run black .` to automatically format all code files.

## Security

**Careful!**
The API is set up to accept requests from any URL at the moment.
This is NOT the correct setting for a private API in production!
See <https://fastapi.tiangolo.com/tutorial/cors/> for more information.
